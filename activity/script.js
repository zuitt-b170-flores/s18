let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log(trainer.pokemon[0] + "! " + "I choose you!")
	}
};
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of bracket notation:");
console.log(trainer.pokemon);
console.log("Result of talk method");
console.log(trainer.talk());

let Pokemon = [
{
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(){
		console.log("Pikachu tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
},
{
	name: "Geodude",
	level: 8,
	health: 16,
	attack: 8,
	tackle: function(){
		console.log("Geodude tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
},
{
	name: "Mewtwo",
	level: 100,
	health: 200,
	attack: 100,
	tackle: function(){
		console.log("Mewtwo tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
}
];
console.log(Pokemon[0]);
console.log(Pokemon[1]);
console.log(Pokemon[2]);

function pokemon(name, level, health, attack){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
// Skills of Pokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health-this.attack));
	if (target.health-this.attack <= 0){
		console.log(target.name + " fainted. ")
		};
	}

}
let Pikachu = new pokemon ("Pikachu", 12, 24, 12);
let Geodude = new pokemon ("Geodude", 8, 16, 8);
let Mewtwo = new pokemon ("Mewtwo", 100, 200, 100);
